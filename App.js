import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import {
  Platform,
  Button,
  TextInput,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Alert,
  Text,
  SafeAreaView,
} from "react-native";
import { DefaultTheme, Provider as PaperProvider } from "react-native-paper";
import HomePage from "./screens/HomePage.js"; //import the method from specified .js file
import Prompts from "./screens/Prompts.js";
import Menu from "./screens/Menu.js"; //was missing originally
import Cards from "./screens/cards.js"; //Screen that  contains the previews of questions
import OathOfA from "./screens/OathofAllegiance.js";


/**
 * import * as ReadingPage from "./screens/ReadingPage.js"; <--- can all functions be used?
 * URL: https://stackoverflow.com/questions/53128479/react-native-importing-function-from-another-file/53128898
 *
 */

function HomeScreen({ navigation }) {
  return (
    <PaperProvider theme={theme}>
      <View style={{ flex: 1, alignItems: "center", paddingTop: 50, backgroundColor: '#ffffff' }}>
        <Image
          source={require('./images/uscis.jpg')}
          style={{ width: 300, height: 200, marginBottom: 100 }} />
        <Text style={{ marginBottom: 50 }}>Naturalization Interview Confidence Environment</Text>
        <Button
          title="Go to details"
          onPress={() => navigation.navigate("Details")}
        />
      </View>
    </PaperProvider>
  );
}

function DetailsScreen({ navigation }) {
  return (
    <SafeAreaView
      style={{ flex: 1, alignItems: "center", justifyContent: "center" }}
    >
      <Text>
        This is the DetailsScreen function from App.js that links to separate
        screens
      </Text>
     
      <Button
        title="Go to HomePage"
        onPress={() => navigation.navigate("HomePage")}
      />
      <Button
        title="Go to ReadingPage"
        onPress={() => navigation.navigate("ReadingPage")}
      />
      <Button
        title="Go to Prompts"
        onPress={() => navigation.navigate("Prompts")}
      />
      
      <Button
        title="Reading Portion"
        onPress={() => navigation.navigate("Cards")}
      />
      <Button
        title="Go to Oath of Allegiance"
        onPress={() => navigation.navigate("OathOfA")}
      />

      <Button title="Go back" onPress={() => navigation.goBack()} />
    </SafeAreaView>
  );
}

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="HomePage" component={HomePage}
          options={{ headerStyle: { backgroundColor: 'lightblue' }, headerTitleAlign: 'center' }} />
        <Stack.Screen name="Menu" component={Menu}
        options={{ headerStyle: { backgroundColor: 'lightblue' }, headerTitleAlign: 'center' }} />
        <Stack.Screen name="Cards" component={Cards}
        options={{ headerStyle: { backgroundColor: 'lightblue' }, headerTitleAlign: 'center' }} />
        <Stack.Screen name="OathOfA" component={OathOfA}
        options={{ headerStyle: { backgroundColor: 'lightblue' }, headerTitleAlign: 'center' }} />
        <Stack.Screen name ="Prompts" component={Prompts}
        options={{ headerStyle: { backgroundColor: 'lightblue' }, headerTitleAlign: 'center' }}/>
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}
export default App;
