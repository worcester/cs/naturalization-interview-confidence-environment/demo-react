import React, { Component } from "react";
//import {useState} from "react";
import { StyleSheet, Text, SafeAreaView, TextInput } from "react-native";
import { Paragraph, Title } from "react-native-paper";
import { AppStyles } from '../styles/AppStyle';

export default function OathOfA({ navigation }) {

  //const [name, setName] = useState("");
  //value={name} onChangeText={(text) => setName}
  return (
    <SafeAreaView style={AppStyles.textContainer}>
      <Title style={AppStyles.heading}>Oath of Allegiance</Title>
      <Text style={AppStyles.text}>Please read the following:{"\n"}</Text>
      <Paragraph style={AppStyles.text}>
        {" "}
        "I hereby declare, on oath, that I absolutely and entirely renounce and
        abjure all allegiance and fidelity to any foreign prince, potentate,
        state, or sovereignty, of whom or which I have heretofore been a subject
        or citizen; that I will support and defend the Constitution and laws of
        the United States of America against all enemies, foreign and domestic;
        that I will bear true faith and allegiance to the same; that I will bear
        arms on behalf of the United States when required by the law; that I
        will perform noncombatant service in the Armed Forces of the United
        States when required by the law; that I will perform work of national
        importance under civilian direction when required by the law; and that I
        take this obligation freely, without any mental reservation or purpose
        of evasion; so help me God."{"\n"}
      </Paragraph>
      <Text style={AppStyles.text}>Please enter your full legal name:</Text>
      <TextInput style={AppStyles.input} placeholder="Enter your name here" secureTextEntry={true} />
    </SafeAreaView>
  );
}
