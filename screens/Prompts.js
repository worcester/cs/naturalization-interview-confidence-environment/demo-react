import React, { Component } from "react";
import { readString, readRemoteFile } from "react-native-csv";
import { StatusBar } from "expo-status-bar";
import sentences from "../assets/sentences.csv";
import { Card, Paragraph, Button } from "react-native-paper";
import { AppStyles } from "../styles/AppStyle";
import {

  StyleSheet,
  View,
  Text,
  TextInput,
  SafeAreaView,
  ScrollView,
} from "react-native";


  export default function Prompts({ navigation }) {
    let prompts = []
    for(let i = 0; i < promptsToRead.length; i++){

      prompts.push(
      <Card style={AppStyles.card}>
        <Card.Content style={AppStyles.cardContent}>
          <Paragraph numberOfLines={1} style={AppStyles.paragraphs}>
            {promptsToRead[i]}
          </Paragraph>
          <Card.Actions>
           
          </Card.Actions>
         </Card.Content>
      </Card>)
    }
    prompts.map((item) => <li key= "{item}"> {item}  </li>);
    return (
    <ScrollView>
      <SafeAreaView>
        <View style = {AppStyles.answerBox}>
        <Text>Use The Space Below To Record Your Submission</Text>
        <Card style={AppStyles.card}>
          <Card.Content>
            <TextInput 
            placeholder="Type In Me!"
            multiline = {true}
            />
          </Card.Content>
        </Card>
        </View>
        <View>
        <ScrollView
        style ={AppStyles.promptBox}>
            {prompts}
        </ScrollView>
        </View>
        <View>
          <Button
            onPress={() => {
               //button function goes here..
            }}
            style={AppStyles.submitBox}
          >
            Submit
          </Button>
        </View>
      </SafeAreaView>
    </ScrollView>
    );
  }

  let promptsToRead = [];
  readRemoteFile(sentences, {
  complete: (results,file)  => {
  for(let i = 0; i < results.data.length; i++){
    promptsToRead.push(results.data[i])}
}});