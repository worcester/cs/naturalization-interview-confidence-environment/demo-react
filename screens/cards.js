import React, { Component } from "react";
import { StyleSheet, ScrollView } from "react-native";
import { Card, Paragraph, Button, Title } from "react-native-paper";
import { SafeAreaView } from "react-native-safe-area-context";
import { AppStyles } from "../styles/AppStyle";
import questionsFileJSON from "../assets/100Q_for_NICE.json";

export default function CardFunction({ navigation }) {
  let questions = []; //create an array for the card components
  for (var index in questionsFileJSON.questionsCollection) { //go thru JSON file and iterate thru indeces 
    //id corresponds to attribute specified in JSON file
    //push card component into array
    questions.push(<Card style={AppStyles.card}
    testID="{index}">
      <Card.Content>
        <Paragraph
          //numberOfLines={1}
          style={AppStyles.paragraphs}
        >
          {(parseInt(index) + 1) + ") " + questionsFileJSON.questionsCollection[index].questionText //store the question text at specified index
          }
        </Paragraph>
        <Card.Actions>
          {/*<Button
            onPress={() => {
              navigation.navigate('Display', {
                card: questionsFileJSON.questionsCollection[1].questionText,
              })
            }}
            style={AppStyles.boldText}
          >
            View My full Question
          </Button>*/}
        </Card.Actions>
      </Card.Content>
    </Card>)
  }

  questions.map((item) => <li key="{item}">{item}</li>);
  return (
    <ScrollView>
      <SafeAreaView style={AppStyles.card}>
        <Title>Questions Page</Title>
        <Paragraph>
          This page contains the interview questions for the user to select
        </Paragraph>
        {questions}
        <SafeAreaView style={AppStyles.cardContent}></SafeAreaView>
      </SafeAreaView>
    </ScrollView>
  );
}
