import * as React from "react";
import {
    View,
    Text,
    TouchableOpacity,
} from "react-native";
//import { Paragraph } from "react-native-paper";

export default function Display({ route, navigation }) {
    const { card } = route.params;

    return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            <Text>card: {JSON.stringify(card)}</Text>
        </View>
    );
}
