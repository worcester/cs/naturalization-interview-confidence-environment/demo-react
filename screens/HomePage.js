import * as React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from "react-native";
import { AppStyles } from '../styles/AppStyle';

export default function HomePage({ navigation }) {
  function handleNavigation(screenName) {
    navigation.navigate(screenName);
  }
  return (
    <View style={AppStyles.screen}>
      <Image
        source={require('../images/uscis.jpg')}
        style={{ width: 300, height: 200, marginBottom: 100 }} />
      <Text>Naturalization Interview Confidence Environment</Text>
      <TouchableOpacity
        onPress={() => handleNavigation('Menu')}
        style={AppStyles.home}>
        <Text style={{ textAlign: 'center' }}>Start</Text>
      </TouchableOpacity>
      <Image
        source={require('../images/WSU.jpg')}
        style={{ width: 100, height: 30, position: "absolute", bottom: 0, alignSelf: "center" }}
      />
    </View>

  );
}
