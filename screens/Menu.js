import * as React from "react";
import {
    StyleSheet,
    View,
    Text,
    Image,
    ImageBackground

} from "react-native";
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import { MenuStyles } from '../styles/MenuStyle';

const Menu = ({ navigation }) => {
    return (

        <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={{ height: 800, paddingTop: 200 }}
        >

            <TouchableOpacity
                onPress={() => navigation.navigate("Prompts")}
                style={MenuStyles.menuCard}
            >
                <Image
                    source={require('../images/book.png')}
                    style={MenuStyles.menuImage}
                />
                <View style={MenuStyles.titleStyle}>
                    <Text style={MenuStyles.cardTitle}>PROMPTS</Text>

                </View>
                <Text style={MenuStyles.bottomText}>
                    Click to Open!
                </Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => navigation.navigate("Cards")}
                style={MenuStyles.menuCard}
            >
                <Image
                    source={require('../images/playing-card.png')}
                    style={MenuStyles.menuImage}
                />
                <View style={MenuStyles.titleImage}>
                    <Text style={MenuStyles.cardTitle}>CARDS</Text>

                </View>
                <Text style={MenuStyles.bottomText}>
                    Click to Open!
                </Text>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={() => navigation.navigate("OathOfA")}
                style={MenuStyles.menuCard}
            >
                <Image
                    source={require('../images/teleprompter.png')}
                    style={MenuStyles.menuImage}
                />
                <View style={MenuStyles.titleStyle}>
                    <Text style={MenuStyles.cardTitle}>OATH OF A</Text>

                </View>
                <Text style={MenuStyles.bottomText}>
                    Click to Open!
                </Text>
            </TouchableOpacity>


        </ScrollView>
    );
}

export default Menu;