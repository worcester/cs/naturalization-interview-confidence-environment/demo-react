#Project Structure

##Description

This is a UML diagram that shows the structure of our project. This is going to be a file that will get modified over time.

[Youtube Tutorial](https://www.youtube.com/watch?v=OmQCU-3KPms)

##Current Model

```plantuml

@startuml
hide empty description

[*] -up-> Home_Screen
Home_Screen -up-> Exam_Selection
Exam_Selection -up-> Preview_Sentence
Preview_Sentence -up-> Read_Aloud
Exam_Selection -up-> Prompt
Prompt -up-> Writing_Portion

@enduml
```

##Testing

```plantuml

@startuml
hide empty description

package "Naturalization Interview Confidence Environment"{
    node "HomePage.js" <<Screen>>{

    }
    node "OathofAllegiance.js" <<Screen>>{

    }
    node "ReadingPage.js" <<Screen>>{

    }
    node "WritingPage.js" <<Screen>>{

    }
    node "ExamTypeSelection.js" <<Screen>>{

    }
    HomePage.js -> OathofAllegiance.js
    OathofAllegiance.js -> ExamTypeSelection.js
    ExamTypeSelection.js -> WritingPage.js
    ExamTypeSelection.js -> ReadingPage.js
}

@enduml
```
