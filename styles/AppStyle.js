import { StyleSheet } from "react-native";

export const AppStyles = StyleSheet.create({
  textContainer: {
    flex: 1,
  },

  text: {
    fontSize: 16,
    textAlign: "left",
  },

  home: {
    backgroundColor: "lightblue",
    height: 40,
    width: 100,
    margin: 50,
    borderWidth: 1,
    padding: 10,
  },

  heading: {
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
  },

  input: {
    height: 40,
    margin: 50,
    borderWidth: 1,
    padding: 10,
    marginTop: 0,
  },
  card: {
    borderRadius: 6,
    border: 20,
    elevation: 3,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: "#333",
    shadowOpacity: 0.3,
    shadowRadius: 4,
    marginHorizontal: 4,
    marginVertical: 10,
  },
  cardContent: {
    backgroundColor: " #d7eff3",
    margin: 4,
  },

  nextScreen: {
    backgroundColor: "lightblue",
    height: 60,
    width: 200,
    margin: 20,
    borderWidth: 1,
    padding: 10,
  },

    paragraphs: {
        fontWeight: "bold", //make the text bold
    },

    boldText: {
        fontWeight: "bold", //make the text bold
    },
    screen: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        paddingTop: 50,
        backgroundColor: '#ffffff'
    },
    promptBox: {
      position: "",
      flex: 3,
      collapsable: "true"
    },
    answerBox: {
      flex: 1,
      justifyContent: "top" ,
      position: "",
    },
    submitBox:{
      flex: 1,
      justifyContent: "bottom" ,
      position: "",
    },
    containerCard: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    cardText: {
        fontSize: 30
    },
    cards: {
        backgroundColor: '#fff'
    },
    marginBottom: 10,
    margineLeft: '2%',
    width: '96%',
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowOffset: {
        width: 3,
        height: 3
    },
    cardImage: {
        width: '100%',
        height: 200,
        resizeMode: 'cover'
    }
});
