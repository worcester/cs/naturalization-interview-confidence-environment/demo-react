import { StyleSheet } from "react-native";

export const MenuStyles = StyleSheet.create({
    menuCard: {
        height: 250,
        elevation: 2,
        backgroundColor: "#FFF",
        marginLeft: 20,
        marginTop: 20,
        borderRadius: 15,
        marginBottom: 10,
        width: 160,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menuImage:{
        width: 100, 
        height: 100, 
        marginTop: 50, 
        marginBottom: 50 
    },
    
    bottomText: {
        paddingHorizontal: 10,
        fontWeight: "bold",
        color: "#87cefa",
        paddingTop: 3
    },

    row: {
        flexDirection: "row",
        paddingTop: 10,
        paddingHorizontal: 10
    },

    cardTitle: {
        fontWeight: "bold"
    },

    titleStyle: {
        flexDirection: "row",
        paddingTop: 10,
        paddingHorizontal: 10
    }
})